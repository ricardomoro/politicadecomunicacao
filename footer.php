    </section>
    <footer class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <a href="#barra-brasil" class="btn btn-link" id="voltar-ao-topo">Voltar ao topo</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-5" id="footer-logo">
                <a href="http://www.ifrs.edu.br/" title="Site do IFRS" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-ifrs-completo.jpg" alt="Logo do IFRS" class="img-responsive" /></a>
            </div>
            <div class="col-xs-12 col-md-7" id="footer-info">
                <div class="separator"></div>
                <div id="footer-info-content">
                    <p><?php bloginfo('title'); ?></p>
                    <p><?php bloginfo('description'); ?></p>
                    <p id="footer-links">
                        <!-- Wordpress -->
                        <a href="http://www.wordpress.org/" target="blank">Desenvolvido com Wordpress<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a> <span class="glyphicon glyphicon-new-window"></span>
                        &mdash;
                        <!-- Código-fonte GPL -->
                        <a href="https://bitbucket.org/ricardomoro/politicadecomunicacao/" target="blank">C&oacute;digo-fonte deste tema sob a licen&ccedil;a GPLv3<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a> <span class="glyphicon glyphicon-new-window"></span>
                        &mdash;
                        <!-- Creative Commons -->
                        <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="blank"><img src="<?php echo get_template_directory_uri(); ?>/img/cc-by-nc-nd.png" alt="M&iacute;dia licenciada sob a Licen&ccedil;a Creative Commons Atribui&ccedil;&atilde;o-N&atilde;oComercial-SemDeriva&ccedil;&otilde;es 4.0 Internacional (abre uma nova p&aacute;gina)" /></a> <span class="glyphicon glyphicon-new-window"></span>
                    </p>
                </div>
            </div>
        </div>
    </footer>
<!-- Javascript -->
<?php wp_footer(); ?>
</body>
</html>
