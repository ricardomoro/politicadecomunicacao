<?php get_header(); ?>


<?php get_template_part('partials/carousel'); ?>

<?php
    global $wp_query;
    $args = array(
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => 'novidades',
            ),
        ),
        'posts_per_page' => 4,
    );
    $args = array_merge($wp_query->query_vars, $args);
    query_posts($args);
?>

<?php if (have_posts()) : ?>
    <div id="home-novidades">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php
                                $novidades_obj = get_category_by_slug('novidades');
                                if ($novidades_obj) :
                                    $novidades_link = get_category_link($novidades_obj->cat_ID);
                                endif;
                            ?>
                            <h2><a href="<?php echo esc_url( $novidades_link ); ?>">Not&iacute;cias</a></h2>
                        </div>
                    </div>
                    <div class="row">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <?php if (has_post_thumbnail()) : ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                <?php endif; ?>
                                <div class="caption">
                                    <p><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>

<?php
    global $wp_query;
    $args = array(
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => 'videos',
            ),
        ),
        'posts_per_page' => 4,
    );
    $args = array_merge($wp_query->query_vars, $args);
    query_posts($args);
?>

<?php if (have_posts()) : ?>
    <div id="home-videos">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php
                                $videos_obj = get_category_by_slug('videos');
                                if ($videos_obj) :
                                    $videos_link = get_category_link($videos_obj->cat_ID);
                                endif;
                            ?>
                            <h2><a href="<?php echo esc_url( $videos_link ); ?>">V&iacute;deos</a></h2>
                        </div>
                    </div>
                    <div class="row">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <?php if (has_post_thumbnail()) : ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                <?php endif; ?>
                                <div class="caption">
                                    <p><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>

<?php
    global $wp_query;
    $args = array(
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => 'faq',
            ),
        ),
        'posts_per_page' => 4,
    );
    $args = array_merge($wp_query->query_vars, $args);
    query_posts($args);
?>
<?php if (have_posts()) : ?>
    <div id="home-faq">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php
                                $faq_obj = get_category_by_slug('faq');
                                if ($faq_obj) :
                                    $faq_link = get_category_link($faq_obj->cat_ID);
                                endif;
                            ?>
                            <h2><a href="<?php echo esc_url( $faq_link ); ?>">Perguntas Frequentes</a></h2>
                        </div>
                    </div>
                    <div class="row">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <a href="<?php the_permalink(); ?>" class="link-faq"><?php the_title(); ?></a>
                        </div>
                    <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>

<div id="home-contato">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Contato</h2>
                    </div>
                </div>
                <div class="row">
                <?php
                    $page = get_page_by_path('contato');
                    $contato = get_post($page->ID);
                    echo do_shortcode($contato->post_content);
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
