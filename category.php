<?php get_header(); ?>

<div class="container">
    <div class="row" id="first-content">
        <div class="col-xs-12">
            <h2><?php single_cat_title('', true); ?></h2>
        </div>
    </div>

    <div class="row">
        <?php while (have_posts()) : the_post(); ?>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="thumbnail">
                    <?php if (has_post_thumbnail()) : ?>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail(); ?>
                        </a>
                    <?php endif; ?>
                    <div class="caption">
                        <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                        <br/>
                        <small class="pull-right"><?php echo get_the_date('d/m/Y'); ?></small>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
</div>

<?php get_footer(); ?>
