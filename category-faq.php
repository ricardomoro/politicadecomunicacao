<?php get_header(); ?>

<div class="container">
    <div class="row" id="first-content">
        <div class="col-xs-12">
            <h2><?php single_cat_title('', true); ?></h2>
        </div>
    </div>

    <div class="row">
        <?php while (have_posts()) : the_post(); ?>
            <div class="col-xs-12 col-sm-6">
                <h2 class="faq-title"><?php the_title(); ?></h2>
                <?php if (has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail(); ?>
                <?php endif; ?>
                <?php
                    global $more;
                    $more = 1;
                ?>
                <?php the_content(); ?>
            </div>
        <?php endwhile; ?>
    </div>
</div>

<?php get_footer(); ?>
