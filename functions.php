<?php
//Registra o menu principal
register_nav_menus(
    array(
        'menu-superior' => 'Menu Superior (Principal)',
    )
);

// Habilita a imagem destacada nos posts.
add_theme_support( 'post-thumbnails' );

// Habilita o tipo de post para imagens.
add_theme_support('post-formats', array('image'));

// Menu do Bootstrap
require_once('inc/wp_bootstrap_navwalker.php');

// Script Conditional
require_once('inc/script_conditional.php');

// Scripts & Styles
require_once('inc/assets.php');

// Template para posts de uma categoria específica
require_once('inc/template_post_category.php');

// Customização do formulário de comentário.
require_once('inc/comment_form_tbs.php');

// Adicionar PrettyPhoto automaticamente.
require_once('inc/prettyphoto_rel.php');
