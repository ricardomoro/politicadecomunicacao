<?php get_header(); ?>

<div class="container">
    <div class="row" id="first-content">
        <div class="col-xs-12">
        <?php while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="post-title"><?php the_title(); ?></h2>
                    <div class="post-content">
                        <?php 
                            if (has_post_thumbnail()) {
                                the_post_thumbnail('full', array('class' => 'post-thumb'));
                            }
                        ?>
                        <div class="post-content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?php comments_template('/comments.php'); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
