<?php get_header(); ?>

<div class="container">
    <?php if (!have_posts()) : ?>
        <div class="alert alert-warning">
            <?php _e('Sorry, no results were found.', 'politicadecomunicacao'); ?>
        </div>
        <?php get_search_form(); ?>
    <?php endif; ?>

    <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part('content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
    <?php endwhile; ?>
</div>

<?php get_footer(); ?>
