<form role="search" method="get" action="<?php echo esc_url( home_url() ); ?>" class="search-form form-inline pull-right">
    <div class="form-group">
        <input type="text" value="<?php echo get_search_query(); ?>" name="s" placeholder="<?php _e( 'Procure em todo site', 'politicadecomunicacao' ); ?>" class="form-control" />
        <button class="btn btn-primary" type="submit" title="Buscar"><span class="glyphicon glyphicon-search"></span></button>
    </div>
</form>
