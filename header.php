<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title><?php wp_title('&laquo;', true, 'right'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico" type="image/x-icon"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php get_template_part('partials/barrabrasil'); ?>
    <div id="fundo-topo">
        <header class="container" id="topo">
            <div class="row">
                <div class="col-xs-12 col-sm-4" id="col-logo">
                    <h1 class="sr-only"><?php bloginfo('name'); ?></h1>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-ifrs.png" id="logo-ifrs" class="hidden-xs hidden-sm" />
                    <a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-politica.png" id="logo-politica" class="img-responsive" alt="<?php bloginfo('name'); ?>"/></a>
                </div>
                <div class="col-xs-12 col-sm-8" id="col-menu">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php get_search_form(); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php get_template_part('partials/menu'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
