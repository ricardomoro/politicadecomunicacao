<?php
function load_styles() {
    // wp_enqueue_style( $handle, $src, $deps, $ver, $media );
    if (WP_DEBUG) {
        wp_enqueue_style( 'css-bootstrap', get_template_directory_uri().'/vendor/bootstrap/dist/css/bootstrap.css', array(), false, 'all' );
        wp_enqueue_style( 'css-bootstrap-theme', get_template_directory_uri().'/vendor/bootstrap/dist/css/bootstrap-theme.css', array(), false, 'all' );
        wp_enqueue_style( 'css-prettyPhoto', get_template_directory_uri().'/vendor/pretty-photo/css/prettyPhoto.css', array(), false, 'screen' );
        wp_enqueue_style( 'fonts', get_template_directory_uri().'/css/fonts.css', array(), false, 'all' );
        wp_enqueue_style( 'main', get_template_directory_uri().'/css/main.css', array(), false, 'all' );
    } else {
        wp_enqueue_style( 'css-bootstrap', get_template_directory_uri().'/vendor/bootstrap/dist/css/bootstrap.min.css', array(), false, 'all' );
        wp_enqueue_style( 'css-bootstrap-theme', get_template_directory_uri().'/vendor/bootstrap/dist/css/bootstrap-theme.min.css', array(), false, 'all' );
        wp_enqueue_style( 'css-prettyPhoto', get_template_directory_uri().'/vendor/pretty-photo/css/prettyPhoto.css', array(), false, 'screen' );
        wp_enqueue_style( 'barra-brasil-fix', get_template_directory_uri().'/css/barra-brasil-fix.css', array(), false, 'all' );
        wp_enqueue_style( 'fonts', get_template_directory_uri().'/css/fonts.css', array(), false, 'all' );
        wp_enqueue_style( 'main', get_template_directory_uri().'/css/main.css', array(), false, 'all' );
    }
}

function load_scripts() {
    if ( ! is_admin() ) {
        wp_deregister_script('jquery');
    }

    // wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
    if (WP_DEBUG) {
        wp_enqueue_script( 'html5shiv', get_template_directory_uri().'/vendor/html5shiv/dist/html5shiv.js', array(), false, false );
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'respond', get_template_directory_uri().'/vendor/respond-minmax/dest/respond.src.js', array(), false, false );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'jquery-custom', get_template_directory_uri().'/vendor/jquery/dist/jquery.js', array(), false, false );
        wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/vendor/bootstrap/dist/js/bootstrap.js', array(), false, false );

        wp_enqueue_script( 'prettyPhoto', get_template_directory_uri().'/vendor/pretty-photo/js/jquery.prettyPhoto.js', array(), false, false );
        wp_enqueue_script( 'prettyPhoto-config', get_template_directory_uri().'/js/prettyPhoto.config.js', array(), false, true );
    } else {
        wp_enqueue_script( 'html5shiv', get_template_directory_uri().'/vendor/html5shiv/dist/html5shiv.min.js', array(), false, false );
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'respond', get_template_directory_uri().'/vendor/respond-minmax/dest/respond.min.js', array(), false, false );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'jquery-custom', get_template_directory_uri().'/vendor/jquery/dist/jquery.min.js', array(), false, false );
        wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/vendor/bootstrap/dist/js/bootstrap.min.js', array(), false, false );

        wp_enqueue_script( 'prettyPhoto', get_template_directory_uri().'/vendor/pretty-photo/js/jquery.prettyPhoto.js', array(), false, false );
        wp_enqueue_script( 'prettyPhoto-config', get_template_directory_uri().'/js/prettyPhoto.config.js', array(), false, true );

        wp_enqueue_script( 'js-barra-brasil', 'http://barra.brasil.gov.br/barra.js?cor=verde', array(), false, true );
    }
}

add_action( 'wp_enqueue_scripts', 'load_styles' );
add_action( 'wp_enqueue_scripts', 'load_scripts' );
