<?php get_header(); ?>

<div class="container">
    <div class="row" id="first-content">
        <div class="col-xs-12 col-md-9">
        <?php while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="post-title"><?php the_title(); ?></h2>
                    <div class="post-content">
                        <?php 
                            if (has_post_thumbnail()) {
                                the_post_thumbnail('full', array('class' => 'post-thumb'));
                            }
                        ?>
                        <div class="post-social<?php echo (!has_post_thumbnail() ? ' pull-right' : ''); ?>">
                            <p>Compartilhe: <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icone-facebook.png" alt="Compartilhar no Facebook"/></a></p>
                        </div>
                        <div class="post-content">
                            <?php the_content(); ?>
                        </div>
                        <div class="post-meta">
                            <p>Publicado em <strong><?php the_date('d/m/Y'); ?></strong>.</p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
        </div>

        <div class="col-xs-12 col-md-3">
            <!-- Outros posts das mesmas categorias. -->
            <?php
                global $post;
                
                $cat_ID = array();
                $categories = get_the_category();
                
                foreach ($categories as $category) {
                    array_push($cat_ID, $category->cat_ID);
                }
                
                $args = array(
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'post_type' => 'post',
                    'numberposts' => 5,
                    'post__not_in' => array($post->ID),
                    'category__in' => $cat_ID,
                );

                $cat_posts = get_posts($args);
            ?>
            <?php if (!empty($cat_posts)) : ?>
                <div class="well">
                    <h2>Conte&uacute;do Relacionado</h2>
                    <?php foreach ($cat_posts as $cat_post) : ?>
                        <div class="thumbnail">
                            <?php if (has_post_thumbnail($cat_post->ID)) : ?>
                                <a href="<?php echo get_permalink($cat_post->ID); ?>">
                                    <?php echo get_the_post_thumbnail($cat_post->ID); ?>
                                </a>
                            <?php endif; ?>
                            <div class="caption">
                                <a href="<?php echo get_permalink($cat_post->ID); ?>" rel="bookmark"><?php echo $cat_post->post_title; ?></a>
                                <hr/>
                                <p class="pull-right"><?php echo get_the_date('d/m/Y', $cat_post->ID); ?></p>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?php comments_template('/comments.php'); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
