# Política de Comunicação

Tema do Wordpress para o hotsite da Política de Comunicação do IFRS.

## Licença

Esse software é distribuído sob a licença [GPL-3.0](http://opensource.org/licenses/GPL-3.0).
