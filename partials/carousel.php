<?php
    global $wp_query;
    $args = array(
        'tax_query' => array(
            array(
                'taxonomy' => 'post_format',
                'field'    => 'slug',
                'terms'    => 'post-format-image',
            ),
        ),
    );
    $args = array_merge($wp_query->query_vars, $args);
    query_posts($args);
?>

<?php if (have_posts()) : ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="home-carousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php for ($i = 0; $i < $wp_query->found_posts; $i++): ?>
                            <li data-target="#home-carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0 ? 'active' : ''); ?>"></li>
                        <?php endfor; ?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="item<?php echo ($wp_query->current_post == 0) ? ' active' : ''; ?>">
                            <div class="carousel-text">
                                <?php the_content(); ?>
                            </div>
                            <div class="carousel-img">
                                <div class="separator"></div>
                            <?php 
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail();
                                }
                            ?>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>
